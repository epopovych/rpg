﻿//
// DirectXPage.xaml.cpp
// Implementation of the DirectXPage.xaml class.
//

#include "pch.h"
#include "DirectXPage.xaml.h"

#include "FileImageSource.h"
#include "Image.h"
#include "ImageContainer.h"
#include "Text.h"
#include "TextFont.h"

#include <sstream>

#include <windows.ui.xaml.media.dxinterop.h>

using namespace GraphicsTest;
namespace GI = Graphics::Implementation;

using namespace Platform;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Graphics::Display;
using namespace Windows::UI::Input;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;


DirectXPage::DirectXPage() :
	m_renderNeeded(true),
	m_lastPointValid(false),
	m_imageSource(L".\\Assets\\jones.png"),
	m_container(nullptr),
	m_text(nullptr)
{
	InitializeComponent();

	//m_renderer = ref new SimpleTextRenderer();

	//m_renderer->Initialize(
	//	Window::Current->CoreWindow,
	//	SwapChainPanel,
	//	DisplayProperties::LogicalDpi
	//	);

	m_scene.CreateDeviceIndependentResources();

	Window::Current->CoreWindow->SizeChanged += 
		ref new TypedEventHandler<CoreWindow^, WindowSizeChangedEventArgs^>(this, &DirectXPage::OnWindowSizeChanged);

	DisplayProperties::LogicalDpiChanged +=
		ref new DisplayPropertiesEventHandler(this, &DirectXPage::OnLogicalDpiChanged);

	DisplayProperties::OrientationChanged +=
        ref new DisplayPropertiesEventHandler(this, &DirectXPage::OnOrientationChanged);

	DisplayProperties::DisplayContentsInvalidated +=
		ref new DisplayPropertiesEventHandler(this, &DirectXPage::OnDisplayContentsInvalidated);
	
	m_eventToken = CompositionTarget::Rendering::add(ref new EventHandler<Object^>(this, &DirectXPage::OnRendering));

	m_scene.CreateDeviceResources();
	m_scene.SetDpi(DisplayProperties::LogicalDpi);

	// TODO: extract to separate method
	auto windowBounds = Window::Current->CoreWindow->Bounds;
	KennyKerr::RectF bounds(windowBounds.Left, windowBounds.Top, windowBounds.Right, windowBounds.Bottom);
	m_scene.UpdateForWindowSizeChange(bounds);

	// TODO: extract to separate method
	// Associate the new swap chain with the SwapChainBackgroundPanel element.
	ComPtr<ISwapChainBackgroundPanelNative> panelNative;
	HRESULT result = reinterpret_cast<IUnknown*>(SwapChainPanel)->QueryInterface(IID_PPV_ARGS(&panelNative));
	if (FAILED(result)) // TODO: use HR macros here
	{
		throw Platform::Exception::CreateException(result);
	}

	result = panelNative->SetSwapChain(m_scene.GetSwapChain());
	if (FAILED(result))
	{
		throw Platform::Exception::CreateException(result);
	}

	//CreateSceneImageContainer();

	CreateTextes();
}


void DirectXPage::OnPointerMoved(Object^ sender, PointerRoutedEventArgs^ args)
{
	//auto currentPoint = args->GetCurrentPoint(nullptr);
	//if (currentPoint->IsInContact)
	//{
	//	if (m_lastPointValid)
	//	{
	//		Windows::Foundation::Point delta(
	//			currentPoint->Position.X - m_lastPoint.X,
	//			currentPoint->Position.Y - m_lastPoint.Y
	//			);
	//		m_renderer->UpdateTextPosition(delta);
	//		m_renderNeeded = true;
	//	}
	//	m_lastPoint = currentPoint->Position;
	//	m_lastPointValid = true;
	//}
	//else
	//{
	//	m_lastPointValid = false;
	//}
}

void DirectXPage::OnPointerReleased(Object^ sender, PointerRoutedEventArgs^ args)
{
	//m_lastPointValid = false;
	//////////////////////////////////////////////////////////////////////////

	//int index = m_container->GetCurrentIndex() < m_container->GetCount() - 1 
	//	? m_container->GetCurrentIndex() + 1
	//	: 0;
	//m_container->SetCurrentIndex(index);
	//m_renderNeeded = true;
	//////////////////////////////////////////////////////////////////////////

	//m_scene.animationManager().CreateImageContainerIndexLinear(*m_container, 5.0, 3.0, false);

	m_scene.animationManager().CreateObjectOpacity(*m_text, 0.5, m_text->Opacity() == 0.0 
		? GI::AnimationOpacityDirection::Show : GI::AnimationOpacityDirection::Hide);
}

void DirectXPage::OnWindowSizeChanged(CoreWindow^ sender, WindowSizeChangedEventArgs^ args)
{	
	auto windowBounds = sender->Bounds;
	KennyKerr::RectF bounds(windowBounds.Left, windowBounds.Top, windowBounds.Right, windowBounds.Bottom);
	m_scene.UpdateForWindowSizeChange(bounds);
	m_renderNeeded = true;
}

void DirectXPage::OnLogicalDpiChanged(Object^ sender)
{
	m_scene.SetDpi(DisplayProperties::LogicalDpi);
	m_renderNeeded = true;
}

void DirectXPage::OnOrientationChanged(Object^ sender)
{
	// don't process orientation change. we work in landscape orientation only so far

	//m_renderer->UpdateForWindowSizeChange();
	//m_scene.CreateDeviceResources();
	//m_renderNeeded = true;
}

void DirectXPage::OnDisplayContentsInvalidated(Object^ sender)
{
	m_scene.ValidateDevice();
	m_renderNeeded = true;
}

void DirectXPage::OnRendering(Object^ sender, Object^ args)
{
	if (m_renderNeeded || m_scene.animationManager().IsAnimationInProcess())
	{
		m_scene.Render();
		m_renderNeeded = false;
	}
}

void DirectXPage::OnPreviousColorPressed(Object^ sender, RoutedEventArgs^ args)
{
	//m_renderer->BackgroundColorPrevious();
	m_renderNeeded = true;
}

void DirectXPage::OnNextColorPressed(Object^ sender, RoutedEventArgs^ args)
{
	//m_renderer->BackgroundColorNext();
	m_renderNeeded = true;
}

void DirectXPage::SaveInternalState(IPropertySet^ state)
{
	//m_renderer->SaveInternalState(state);
}

void DirectXPage::LoadInternalState(IPropertySet^ state)
{
	//m_renderer->LoadInternalState(state);
}

void DirectXPage::CreateSceneImage()
{
	auto image = std::unique_ptr<GI::Image>(new GI::Image(&m_imageSource));
	m_scene.AddObject(image);
}

void DirectXPage::CreateSceneImageContainer()
{
	auto container = std::unique_ptr<GI::ImageContainer>(new GI::ImageContainer());

	//auto source = std::unique_ptr<GI::FileImageSource>(new GI::FileImageSource())
	const wchar_t* filename = L".\\Assets\\Character\\jm";
	const wchar_t* fileext = L".png";
	for (int i = 0; i < 12; ++i)
	{
		std::wostringstream stream;
		if (i < 10)
			stream << filename << L"0" << i << fileext;
		else
			stream << filename << i << fileext;

		auto source = std::unique_ptr<GI::ImageSource>(new GI::FileImageSource(stream.str()));
		container->AddImageSource(source);
	}

	container->SetCurrentIndex(0);

	m_container = container.get();
	m_scene.AddObject(container);	
}

void DirectXPage::CreateTextes()
{
	m_font.reset(new GI::TextFont(L"Chiller", 60.0, m_scene));
	auto text = make_unique(new GI::Text(*m_font.get()));
	text->SetPosition(KennyKerr::Point2F(100.0, 150.0));
	text->SetForegroundColor(KennyKerr::Color(D2D1::ColorF(D2D1::ColorF::CornflowerBlue)));
	text->SetContent(L"Hello World!");
	text->SetWidth(800.0);
	text->CreateDeviceResources(&m_scene);

	m_text = text.get();
	m_scene.AddObject(text);
}
