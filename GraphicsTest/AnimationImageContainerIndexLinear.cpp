#include "pch.h"
#include "AnimationImageContainerIndexLinear.h"

#include "ImageContainer.h"
#include "AnimationManager.h"

using namespace Graphics::Implementation;

AnimationImageContainerIndexLinear::AnimationImageContainerIndexLinear(
	const AnimationManager& manager,
	ImageContainer& imageContainer, 
	double speed, /*indices per sec*/ 
	double length /*secs*/)

	: m_imageContainer(imageContainer)
	, m_speed(speed)
	, m_length(length)
{
	m_variable = manager.manager().CreateAnimationVariable(0.0);

	auto finalIndexValue = m_length * m_speed;
	m_transition = manager.transitionLibrary().CreateLinearTransition(m_length, finalIndexValue);

	m_storyboard = manager.manager().CreateStoryboard();
	m_storyboard.AddTransition(m_variable, m_transition);
}

void AnimationImageContainerIndexLinear::Start(double time)
{
	m_storyboard.Schedule(time);
}

void AnimationImageContainerIndexLinear::Update(double time)
{
	auto newIndex =  m_variable.GetIntegerValue() % m_imageContainer.GetCount();
	m_imageContainer.SetCurrentIndex(newIndex);

	if (m_variable.GetIntegerValue() >= m_variable.GetFinalIntegerValue())
	{
		Stop();
	}
}
