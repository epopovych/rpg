#include "pch.h"
#include "FileImageSource.h"

using namespace DX;
using namespace Graphics::Implementation;

FileImageSource::FileImageSource(const std::wstring& filename)
	: m_filename(filename)
{	
}

Direct2D::Bitmap1 FileImageSource::GetBitmap(IRenderingContext* context)
{
	if (m_d2dBitmap.Get() == nullptr)
	{
		m_d2dBitmap = LoadImageFromFile(context, m_filename);
	}

	return m_d2dBitmap;
}

Direct2D::Bitmap1 FileImageSource::LoadImageFromFile(IRenderingContext* context, const std::wstring& filename)
{
	auto decoder = context->wicFactory().CreateDecoderFromFile(filename.c_str(), 
		Wic::DecodeCacheOption::OnLoad);

	// create frame
	auto frame = decoder.GetFrame();

	// create converter
	auto converter = context->wicFactory().CreateFormatConverter();
	converter.Initialize(frame);

	// create bitmap
	Direct2D::BitmapProperties1 properties(
		Direct2D::BitmapOptions::None, 
		PixelFormat(Dxgi::Format::B8G8R8A8_UNORM, AlphaMode::Premultipled),
		context->dpi(), context->dpi());
	return context->d2dDeviceContext().CreateBitmapFromWicBitmap1(converter, properties);
}
