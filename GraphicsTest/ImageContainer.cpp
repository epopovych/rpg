#include "pch.h"
#include "ImageContainer.h"

using namespace Graphics::Implementation;
using namespace DX;

ImageContainer::ImageContainer()
	: m_currentIndex(-1)
{
}

void ImageContainer::AddImageSource(std::unique_ptr<ImageSource>& source)
{
	if (source.get() != nullptr)
		m_sources.push_back(std::move(source));
}

int ImageContainer::GetCount() const
{
	return m_sources.size();
}

int ImageContainer::GetCurrentIndex() const
{
	return m_currentIndex;
}

void ImageContainer::SetCurrentIndex(int index)
{
	ASSERT(index >= 0 && static_cast<size_t>(index) < m_sources.size());
	m_currentIndex = index;
}

void ImageContainer::Render(IRenderingContext* context)
{
	if (m_currentIndex == -1 || m_sources.size() == 0)
		return;

	if (!UpdateCurrentBitmap(context))
		return;

	SizeF size = m_d2dCurrentBitmap->GetSize();
	RectF destRect;
	destRect.Left = m_position.X;
	destRect.Top = m_position.Y;
	destRect.Right = destRect.Left + size.Width;
	destRect.Bottom = destRect.Top + size.Height;

	context->d2dDeviceContext().DrawBitmap(m_d2dCurrentBitmap, destRect, m_opacity);
}

bool ImageContainer::UpdateCurrentBitmap(IRenderingContext* context)
{
	if (m_currentIndex == -1 || m_sources.size() == 0)
	{
		m_d2dCurrentBitmap.Reset();
		return false;
	}

	auto source = m_sources[m_currentIndex].get();
	m_d2dCurrentBitmap = source->GetBitmap(context);
	return true;
}
