#include "pch.h"
#include "Object.h"

using namespace Graphics::Implementation;

Object::Object()
	: m_opacity(1.0f)
	, m_isVisible(true)
{
}

DX::Point2F Object::Position() const
{
	return m_position;
}

void Object::SetPosition(const DX::Point2F& val)
{
	m_position = val;
}

float Object::Opacity() const
{
	return m_opacity;
}

void Object::SetOpacity(float val)
{
	m_opacity = val;
}

bool Object::IsVisible() const
{
	return m_isVisible;
}

void Object::SetIsVisible(bool val)
{
	m_isVisible = val;
}

bool Object::IsInside(const DX::Point2F& point) const
{
	return false;
}

void Object::CreateWindowSizeDependentResources(IRenderingContext* context)
{
}

void Object::CreateDeviceResources(IRenderingContext* context)
{
}

void Object::CreateDeviceIndependentResources(IRenderingContext* context)
{
}