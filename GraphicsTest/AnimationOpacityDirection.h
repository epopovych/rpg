#pragma once;

namespace Graphics
{
	namespace Implementation
	{
		enum class AnimationOpacityDirection
		{
			Hide,
			Show
		};
	}
}