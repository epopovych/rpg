#include "pch.h"
#include "Image.h"

using namespace DX;
using namespace Graphics::Implementation;


Image::Image(ImageSource* source)
	: m_source(source)
{
}

void Image::Render(IRenderingContext* context)
{
	if (m_d2dBitmap.Get() == nullptr)
	{
		m_d2dBitmap = m_source->GetBitmap(context);
	}

	SizeF size = m_d2dBitmap->GetSize();
	RectF destRect;
	destRect.Left = m_position.X;
	destRect.Top = m_position.Y;
	destRect.Right = destRect.Left + size.Width;
	destRect.Bottom = destRect.Top + size.Height;

	context->d2dDeviceContext().DrawBitmap(m_d2dBitmap, destRect, m_opacity);
}

bool Image::IsInside(const DX::Point2F& point) const
{
	if (m_d2dBitmap.Get() != nullptr)
	{
		SizeF size = m_d2dBitmap->GetSize();
		DX::RectF bounds(
			m_position.X, 
			m_position.Y, 
			m_position.X + size.Width, 
			m_position.Y + size.Height);

		return (point.X >= bounds.Left && point.X <= bounds.Right &&
			point.Y >= bounds.Top && point.Y <= bounds.Bottom);
	}
	else 
	{
		return false;
	}
}
