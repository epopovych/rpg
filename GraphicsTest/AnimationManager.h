#pragma once

#include <vector>
#include <memory>

#include "Animation.h"
#include "AnimationOpacityDirection.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class ImageContainer;

		typedef std::unique_ptr<Animation> AnimationPtr;

		class AnimationManager
		{
		public:
			AnimationManager();

			DX::Wam::Manager manager() const;
			DX::Wam::TransitionLibrary transitionLibrary() const;

			void Update();

			bool IsAnimationInProcess() const;

			Animation* CreateImageContainerIndexLinear(
				ImageContainer& imageContainer, 
				double speed, /*indices per sec*/
				double length /*secs*/);

			Animation* CreateObjectOpacity(
				Object& object,
				double length, /* secs */
				AnimationOpacityDirection direction);

		private:
			void AddAnimation(Animation* animation);
			void StartAnimation(Graphics::Implementation::Animation* animation);

			DX::Wam::Manager m_manager;
			DX::Wam::TransitionLibrary m_transitionLibrary;
			DX::Wam::Timer m_timer;

			std::vector<AnimationPtr> m_animations;
		};
	}
}