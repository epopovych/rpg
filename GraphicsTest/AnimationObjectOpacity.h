#pragma once

#include "Animation.h"
#include "AnimationOpacityDirection.h"

namespace Graphics
{
	namespace Implementation
	{
		class AnimationObjectOpacity : public Animation
		{
		public:
			AnimationObjectOpacity(
				const AnimationManager& manager,
				Object& object,
				double length /* secs */,
				AnimationOpacityDirection direction);

			virtual void Start(double time) override;
			virtual void Update(double time) override;
			virtual bool IsFinished() const;

		private:
			float GetStartValue(AnimationOpacityDirection direction);
			float GetEndValue(AnimationOpacityDirection direction);

			DX::Wam::Variable m_variable;
			DX::Wam::Transition m_transition;
			DX::Wam::Storyboard m_storyboard;

			Graphics::Implementation::Object& m_object;
		};
	}
}