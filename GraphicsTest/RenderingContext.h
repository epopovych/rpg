#pragma once

namespace Graphics
{
	namespace Implementation
	{
		class IRenderingContext
		{
		public:
			virtual DX::RectF size() const = 0;
			virtual float dpi() const = 0;

			virtual DX::Direct2D::Factory1 d2dFactory() = 0;			
			virtual DX::Wic::Factory2 wicFactory() = 0;
			virtual DX::DirectWrite::Factory1 writeFactory() = 0;

			virtual DX::Direct3D::Device1 d3dDevice() = 0;
			virtual DX::Direct2D::Device d2dDevice() = 0;
			virtual DX::Direct2D::DeviceContext d2dDeviceContext() = 0;			
			virtual DX::Dxgi::SwapChain1 dxgiSwapChain() = 0;
			virtual DX::Direct2D::Bitmap1 d2dTargetBitmap() = 0;
		};
	}
}