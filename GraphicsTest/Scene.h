#pragma once

#include <vector>
#include <memory>

#include "RenderingContext.h"
#include "Object.h"
#include "AnimationManager.h"

namespace Graphics
{
	namespace Implementation
	{
		class Scene : public IRenderingContext
		{
		public:
			typedef std::unique_ptr<Object> ObjectPtr;

			Scene();
			~Scene();

			void CreateDeviceIndependentResources();
			void CreateDeviceResources();
			void CreateWindowSizeDependentResources();
			void HandleDeviceLost();
			void SetDpi(float dpi);
			void SetSize(const DX::RectF& size);
			void UpdateForWindowSizeChange(const DX::RectF& newSize);
			void ValidateDevice();

			void Render();

			void AddObject(std::unique_ptr<Object>& object);
			template <typename T> 
			void AddObject(std::unique_ptr<T>& object)
			{
				ASSERT(object.get() != nullptr);
				m_objects.push_back(std::move(object));
			}

			Object* GetObjectByPoint(const DX::Point2F& point);

			DX::Color BackgroundColor() const { return m_backgroundColor; }
			void SetBackgroundColor(const DX::Color& color) { m_backgroundColor = color; }

			IDXGISwapChain* GetSwapChain();

			AnimationManager& animationManager();

			virtual DX::RectF size() const override { return m_size; }
			virtual float dpi() const { return m_dpi; }

			virtual DX::Direct2D::Factory1 d2dFactory() override { return m_d2dFactory; }
			virtual DX::Wic::Factory2 wicFactory() override { return m_wicFactory; }
			virtual DX::DirectWrite::Factory1 writeFactory() override { return m_writeFactory; }

			virtual DX::Direct3D::Device1 d3dDevice() override { return m_d3dDevice; }
			virtual DX::Direct2D::Device d2dDevice() override { return m_d2dDevice; }
			virtual DX::Direct2D::DeviceContext d2dDeviceContext() override { return m_d2dDeviceContext; }
			virtual DX::Dxgi::SwapChain1 dxgiSwapChain() override { return m_dxgiSwapChain; }
			virtual DX::Direct2D::Bitmap1 d2dTargetBitmap() override { return m_d2dTargetBitmap; }

		private:
			float ConvertDipsToPixels(float dips);
			void Draw();
			
			std::vector<ObjectPtr> m_objects;

			DX::Color m_backgroundColor;

			float m_dpi;
			DX::RectF m_size;

			DX::Direct2D::Factory1 m_d2dFactory;			
			DX::Wic::Factory2 m_wicFactory;
			DX::DirectWrite::Factory1 m_writeFactory;

			DX::Direct3D::Device1 m_d3dDevice;
			DX::Direct2D::Device m_d2dDevice;
			DX::Direct2D::DeviceContext m_d2dDeviceContext;			
			DX::Dxgi::SwapChain1 m_dxgiSwapChain;
			DX::Direct2D::Bitmap1 m_d2dTargetBitmap;

			AnimationManager m_animationManager;
		};
	}

}