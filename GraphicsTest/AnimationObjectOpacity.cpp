#include "pch.h"

#include "AnimationObjectOpacity.h"

using namespace Graphics::Implementation;

const double ACCELERATION_RATIO = 0.2;
const double DECELERATION_RATIO = 0.2;

AnimationObjectOpacity::AnimationObjectOpacity(const AnimationManager& manager, 
											   Object& object, 
											   double length,
											   AnimationOpacityDirection direction)
	: m_object(object)
{
	m_variable = manager.manager().CreateAnimationVariable(GetStartValue(direction));
	m_transition = manager.transitionLibrary().CreateAccelerateDecelerateTransition(
		length, 
		GetEndValue(direction), 
		ACCELERATION_RATIO, 
		DECELERATION_RATIO);
	m_storyboard = manager.manager().CreateStoryboard();
	m_storyboard.AddTransition(m_variable, m_transition);
}

void AnimationObjectOpacity::Start(double time)
{
	m_storyboard.Schedule(time);
}

void AnimationObjectOpacity::Update(double time)
{
	double newOpacity = m_variable.GetValue();
	m_object.SetOpacity(static_cast<float>(newOpacity));
}

float AnimationObjectOpacity::GetStartValue(AnimationOpacityDirection direction)
{
	switch (direction)
	{
	case AnimationOpacityDirection::Hide:
		return 1.0;
	case AnimationOpacityDirection::Show:
	default:
		return 0.0;
	}
}

float AnimationObjectOpacity::GetEndValue(AnimationOpacityDirection direction)
{
	switch (direction)
	{
	case AnimationOpacityDirection::Hide:
		return 0.0;
	case AnimationOpacityDirection::Show:
	default:
		return 1.0;
	}
}

bool AnimationObjectOpacity::IsFinished() const
{
	return __super::IsFinished() || m_storyboard.GetStatus() == DX::Wam::StoryboardStatus::Finished;
}
