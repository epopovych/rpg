﻿#pragma once

#include "DirectXPage.g.h"
#include "Scene.h"
#include "FileImageSource.h"
#include "TextFont.h"

namespace Graphics
{
	namespace Implementation
	{
		class ImageContainer;
		class Text;
	}	
}

namespace GraphicsTest
{
	/// <summary>
	/// A DirectX page that can be used on its own.  Note that it may not be used within a Frame.
	/// </summary>
    [Windows::Foundation::Metadata::WebHostHidden]
	public ref class DirectXPage sealed
	{
	public:
		DirectXPage();

		void OnPreviousColorPressed(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void OnNextColorPressed(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);

		void SaveInternalState(Windows::Foundation::Collections::IPropertySet^ state);
		void LoadInternalState(Windows::Foundation::Collections::IPropertySet^ state);

	private:
		void OnPointerMoved(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ args);
		void OnPointerReleased(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ args);
		void OnWindowSizeChanged(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::WindowSizeChangedEventArgs^ args);
		void OnLogicalDpiChanged(Platform::Object^ sender);
		void OnOrientationChanged(Platform::Object^ sender);
		void OnDisplayContentsInvalidated(Platform::Object^ sender);
		void OnRendering(Object^ sender, Object^ args);

		void CreateSceneImage();
		void CreateSceneImageContainer();
		void CreateTextes();

		Windows::Foundation::EventRegistrationToken m_eventToken;

		Graphics::Implementation::FileImageSource m_imageSource;
		std::unique_ptr<Graphics::Implementation::TextFont> m_font;
		Graphics::Implementation::Scene m_scene;
		bool m_renderNeeded;

		Graphics::Implementation::ImageContainer* m_container;
		Graphics::Implementation::Text* m_text;

		Windows::Foundation::Point m_lastPoint;
		bool m_lastPointValid;
	};
}
