#include "pch.h"
#include "TextFont.h"
#include "RenderingContext.h"

using namespace DX;
using namespace Graphics::Implementation;
using namespace std;

TextFont::TextFont(const std::wstring& fontFamilyName, float fontSize, IRenderingContext& context)
	: m_fontFamilyName(fontFamilyName)
	, m_fontSize(fontSize)
{
	CreateTextFormat(context);
}

std::wstring& TextFont::FontFamilyName()
{
	return m_fontFamilyName;
}

float TextFont::FontSize() const
{
	return m_fontSize;
}

void TextFont::SetFontSize(float size)
{
	ASSERT(size > 0);

	m_fontSize = size;
}

DX::DirectWrite::TextFormat& TextFont::textFormat()
{
	return m_format;
}

void TextFont::CreateTextFormat(IRenderingContext& context)
{
	m_format = context.writeFactory().CreateTextFormat(m_fontFamilyName.c_str(), m_fontSize);
}
