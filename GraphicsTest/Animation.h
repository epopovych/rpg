#pragma once

namespace Graphics 
{
	namespace Implementation 
	{
		class Animation
		{
		public:
			Animation() : m_isFinished(false) {};

			virtual void Start(double time) = 0;
			virtual void Update(double time) = 0;
			virtual bool IsFinished() const { return m_isFinished; };
			virtual void Stop() { m_isFinished = true; };

		protected:
			bool m_isFinished;
		};
	}
}