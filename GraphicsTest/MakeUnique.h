#pragma once

#include <memory>

template<typename T>
std::unique_ptr<T> make_unique(T* ptr)
{
	return std::unique_ptr<T>(ptr);
}