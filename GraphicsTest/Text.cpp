#include "pch.h"
#include "Text.h"
#include "TextFont.h"

using namespace DX;
using namespace Graphics::Implementation;


Text::Text(TextFont& font)
	: m_font(font)
	, m_width(0.0)
{
}

std::wstring Text::Content() const
{
	return m_content;
}

void Text::SetContent(const std::wstring& content)
{
	m_content = content;
}

DX::Color Text::ForegroundColor() const
{
	DX::Color color;
	if (m_foregroundBrush.Get() != nullptr)
		color = m_foregroundBrush.GetColor();
	return color;
}

void Text::SetForegroundColor(const DX::Color& color)
{
	if (m_foregroundBrush.Get() != nullptr)
		m_foregroundBrush.SetColor(color);

	m_foregroundColor = color;
}

void Text::Render(IRenderingContext* context)
{
	m_foregroundBrush.SetOpacity(Opacity());
	if (m_font.textFormat().Get() != nullptr && m_foregroundBrush.Get() != nullptr)
	{
		DX::RectF layoutRect(m_position.X, m_position.Y, m_position.X + m_width, m_position.Y);

		context->d2dDeviceContext().DrawText(
			m_content.c_str(), 
			m_content.size(), 
			m_font.textFormat(),
			layoutRect,
			m_foregroundBrush);
	}
}

void Text::CreateDeviceResources(IRenderingContext* context)
{
	m_foregroundBrush.Reset();
	m_foregroundBrush = context->d2dDeviceContext().CreateSolidColorBrush(m_foregroundColor);
}

float Text::Width() const
{
	return m_width;
}

void Text::SetWidth(float width)
{
	m_width = width;
}
