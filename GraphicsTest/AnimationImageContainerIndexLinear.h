#pragma once

#include "Animation.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class AnimationManager;
		class ImageContainer;

		class AnimationImageContainerIndexLinear : public Animation
		{
		public:
			AnimationImageContainerIndexLinear(
				const AnimationManager& manager,
				ImageContainer& imageContainer,
				double speed, /*indices per sec*/
				double length /*secs*/);

			virtual void Start(double time) override;
			virtual void Update(double time) override;

		private:
			ImageContainer& m_imageContainer;
			double m_speed;
			double m_length;

			DX::Wam::Variable m_variable;
			DX::Wam::Transition m_transition;
			DX::Wam::Storyboard m_storyboard;
		};
	}
}