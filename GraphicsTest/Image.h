#pragma once

#include "Object.h"
#include "ImageSource.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class Image : public Object
		{
		public:
			Image(ImageSource* source);

			virtual void Render(IRenderingContext* context) override;

			virtual bool IsInside(const DX::Point2F& point) const override;

		private:
			ImageSource* m_source;
			DX::Direct2D::Bitmap1 m_d2dBitmap;
		};
	}
}