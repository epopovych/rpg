#pragma once

#include <memory>

#include "RenderingContext.h"

namespace Graphics
{
	namespace Implementation
	{
		class Object
		{
		public:
			Object();

			virtual void Render(IRenderingContext* context) = 0;

			DX::Point2F Position() const;
			void SetPosition(const DX::Point2F& val);

			float Opacity() const;
			void SetOpacity(float val);

			bool IsVisible() const;
			void SetIsVisible(bool val);

			virtual void CreateDeviceIndependentResources(IRenderingContext* context);
			virtual void CreateDeviceResources(IRenderingContext* context);
			virtual void CreateWindowSizeDependentResources(IRenderingContext* context);

			virtual bool IsInside(const DX::Point2F& point) const;

		protected:
			DX::Point2F m_position;
			float m_opacity;
			bool m_isVisible;
		};
	}
}