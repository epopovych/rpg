#include "pch.h"
#include "Scene.h"

#include <math.h>
#include <algorithm>

using namespace DX;
using namespace std;
using namespace Graphics::Implementation;

Scene::Scene()
	: m_dpi(-1.0f)
{

}

Scene::~Scene()
{
	m_writeFactory.Reset();
	m_wicFactory.Reset();
	m_d2dFactory.Reset();
}

void Scene::CreateDeviceIndependentResources()
{
	m_d2dFactory = Direct2D::CreateFactory();
	m_wicFactory = Wic::CreateFactory();
	m_writeFactory = DirectWrite::CreateFactory();

	std::for_each(std::begin(m_objects), std::end(m_objects), [&](const ObjectPtr& object) {
		object->CreateDeviceIndependentResources(this);
	});
}

void Scene::CreateDeviceResources()
{	
	m_d3dDevice = Direct3D::CreateDevice();
	m_d2dDevice = m_d2dFactory.CreateDevice(m_d3dDevice);
	m_d2dDeviceContext = m_d2dDevice.CreateDeviceContext();

	std::for_each(std::begin(m_objects), std::end(m_objects), [&](const ObjectPtr& object) {
		object->CreateDeviceResources(this);
	});
}

void Scene::Render()
{
	m_animationManager.Update();

	m_d2dDeviceContext.BeginDraw();

	//////////////////////////////////////////////////////////////////////////
	// Code from wizard generated standard app. Should we use orientationTransform?
	//////////////////////////////////////////////////////////////////////////
	//// Position the rendered text.
	//Matrix3x2F translation = Matrix3x2F::Translation(
	//	m_windowBounds.Width / 2.0f - m_textMetrics.widthIncludingTrailingWhitespace / 2.0f + m_textPosition.X,
	//	m_windowBounds.Height / 2.0f - m_textMetrics.height / 2.0f + m_textPosition.Y
	//	);

	//// Note that the m_orientationTransform2D matrix is post-multiplied here
	//// in order to correctly orient the text to match the display orientation.
	//// This post-multiplication step is required for any draw calls that are
	//// made to the swap chain's target bitmap. For draw calls to other targets,
	//// this transform should not be applied.
	//m_d2dContext->SetTransform(translation * m_orientationTransform2D);

	Draw();

	auto hr = m_d2dDeviceContext.EndDraw();
	if (hr != D2DERR_RECREATE_TARGET)
	{
		HR(hr);
	}

	hr = m_dxgiSwapChain.Present();

	// TODO: extract that to inline method
	if (hr == DXGI_ERROR_DEVICE_REMOVED)
	{
		HandleDeviceLost();
	}
	else
	{
		HR(hr);
	}
}

void Scene::CreateWindowSizeDependentResources()
{
	unsigned int windowWidth = static_cast<unsigned int>(ConvertDipsToPixels(m_size.Width()));
	unsigned int windowHeight = static_cast<unsigned int>(ConvertDipsToPixels(m_size.Height()));

	// NOTE: swap windowWidth and windowHeight here if we need to support portrait mode. we won't so far

	if (m_dxgiSwapChain)
	{
		auto hr = m_dxgiSwapChain.ResizeBuffers(windowWidth, windowHeight);
		if (hr == DXGI_ERROR_DEVICE_REMOVED)
		{
			HandleDeviceLost();
			return;
		}
		else
		{
			HR(hr);
		}
	}
	else
	{
		auto dxgiFactory = m_d3dDevice.GetDxgiFactory();
		ASSERT(dxgiFactory);

		Dxgi::SwapChainDescription1 swapChainDesc;
		swapChainDesc.Width = windowWidth;
		swapChainDesc.Height = windowHeight;
		swapChainDesc.Format = Dxgi::Format::B8G8R8A8_UNORM;
		swapChainDesc.Stereo = false;
		swapChainDesc.Sample.Count = 1;
		swapChainDesc.Sample.Quality = 0;
		swapChainDesc.BufferUsage = Dxgi::Usage::RenderTargetOutput;
		swapChainDesc.BufferCount = 2;
		swapChainDesc.Scaling = Dxgi::Scaling::Stretch;
		swapChainDesc.SwapEffect = Dxgi::SwapEffect::FlipSequential;

		m_dxgiSwapChain = dxgiFactory.CreateSwapChainForComposition(m_d3dDevice, swapChainDesc);
		ASSERT(m_dxgiSwapChain);

		m_d3dDevice.AsDxgi()->SetMaximumFrameLatency(1);
	}

	Direct2D::BitmapProperties1 bitmapProperties(
		Direct2D::BitmapOptions::Target | Direct2D::BitmapOptions::CannotDraw,
		PixelFormat(Dxgi::Format::B8G8R8A8_UNORM, AlphaMode::Premultipled),
		m_dpi, m_dpi);

	auto surface = m_dxgiSwapChain.GetBuffer();
	ASSERT(surface);
	m_d2dTargetBitmap = m_d2dDeviceContext.CreateBitmapFromDxgiSurface(surface, bitmapProperties);
	ASSERT(m_d2dTargetBitmap);

	m_d2dDeviceContext.SetTarget(m_d2dTargetBitmap);

	m_d2dDeviceContext.SetTextAntialiasMode(Direct2D::TextAntialiasMode::Grayscale);

	std::for_each(std::begin(m_objects), std::end(m_objects), [&](const ObjectPtr& object) {
		object->CreateWindowSizeDependentResources(this);
	});
}

void Scene::SetDpi(float dpi)
{
	if (dpi != m_dpi)
	{
		m_dpi = dpi;
		m_d2dDeviceContext->SetDpi(m_dpi, m_dpi);

		// if size resources are already created
		// UpdateForWindowSizeChange()
	}
}

void Scene::UpdateForWindowSizeChange(const DX::RectF& newSize)
{
	if (m_size.Width() != newSize.Width() || m_size.Height() != newSize.Height())
	{
		m_size = newSize;
		m_d2dDeviceContext->SetTarget(nullptr);

		CreateWindowSizeDependentResources();
	}
}

void Scene::HandleDeviceLost()
{
	float dpi = m_dpi;
	m_dpi = -1.0f;
	m_size.Left = m_size.Right = m_size.Bottom = m_size.Top = 0.0f;
	m_dxgiSwapChain.Reset();

	CreateDeviceResources();
	SetDpi(dpi);
}

void Scene::ValidateDevice()
{
	DXGI_ADAPTER_DESC deviceDesc;
	HR(m_d3dDevice.AsDxgi().GetAdapter()->GetDesc(&deviceDesc));

	Dxgi::Factory2 dxgiFactory = Dxgi::CreateFactory();
	Dxgi::Adapter currentAdapter;
	HR(dxgiFactory->EnumAdapters(0, currentAdapter.GetAddressOf()));
	DXGI_ADAPTER_DESC currentDesc;
	HR(currentAdapter->GetDesc(&currentDesc));

	if (deviceDesc.AdapterLuid.LowPart != currentDesc.AdapterLuid.LowPart ||
		deviceDesc.AdapterLuid.HighPart != currentDesc.AdapterLuid.HighPart ||
		FAILED(m_d3dDevice->GetDeviceRemovedReason()))
	{
		HandleDeviceLost();
	}
}

float Scene::ConvertDipsToPixels(float dips)
{
	static const float dipsPerInch = 96.0f;
	return floor(dips * m_dpi / dipsPerInch + 0.5f); // Round to nearest integer.
}

void Scene::Draw()
{
	m_d2dDeviceContext.Clear(m_backgroundColor);

	for_each(begin(m_objects), end(m_objects), 
		[this](const ObjectPtr& object) { 
			if (object->IsVisible())
				object->Render(this); 
		} );
}

IDXGISwapChain* Scene::GetSwapChain()
{
	return m_dxgiSwapChain.Get();
}

void Scene::SetSize(const DX::RectF& size)
{
	m_size = size;

	// if size resources are already created
	// UpdateForWindowSizeChange()
}

Object* Scene::GetObjectByPoint(const DX::Point2F& point)
{
	auto res = find_if(m_objects.rbegin(), m_objects.rend(), [&](const ObjectPtr& object) -> bool 
	{
		return object->IsInside(point);
	});

	return res != m_objects.rend() 
		? (*res).get() 
		: nullptr;
}

void Scene::AddObject(std::unique_ptr<Object>& object)
{
	ASSERT(object.get() != nullptr);
	m_objects.push_back(std::move(object));
}

AnimationManager& Graphics::Implementation::Scene::animationManager()
{
	return m_animationManager;
}