#include "pch.h"
#include "AnimationManager.h"
#include "AnimationImageContainerIndexLinear.h"
#include "AnimationObjectOpacity.h"

#include <algorithm>

using namespace std;
using namespace Graphics::Implementation;

AnimationManager::AnimationManager()
{
	m_manager = DX::Wam::CreateManager();
	m_transitionLibrary = DX::Wam::CreateTransitionLibrary();
	m_timer = DX::Wam::CreateTimer();
}

DX::Wam::Manager AnimationManager::manager() const
{
	return m_manager;
}

DX::Wam::TransitionLibrary AnimationManager::transitionLibrary() const
{
	return m_transitionLibrary;
}

void AnimationManager::Update()
{
	double currentTime = m_timer.GetTime();
	m_manager.Update(currentTime);

	for_each(begin(m_animations), end(m_animations), [&](AnimationPtr& animation) 
		{ animation->Update(currentTime); } );

	m_animations.erase(
		remove_if(begin(m_animations), end(m_animations), [](AnimationPtr& animation) 
			{ return animation->IsFinished(); } ),
		end(m_animations));
}

void AnimationManager::AddAnimation(Animation* animation)
{
	m_animations.push_back(make_unique<Animation>(animation));
}

void AnimationManager::StartAnimation(Graphics::Implementation::Animation* animation)
{
	animation->Start(m_timer.GetTime());
}

bool Graphics::Implementation::AnimationManager::IsAnimationInProcess() const
{
	return m_animations.size() > 0;
}

Animation* AnimationManager::CreateImageContainerIndexLinear(
	ImageContainer& imageContainer, 
	double speed, /*indices per sec*/ 
	double length /*secs*/)
{
	auto animation = new AnimationImageContainerIndexLinear(*this, imageContainer, speed, length);
	AddAnimation(animation);
	StartAnimation(animation);
	return animation;
}

Animation* Graphics::Implementation::AnimationManager::CreateObjectOpacity(
	Object& object, 
	double length, /* secs */ 
	AnimationOpacityDirection direction)
{
	auto animation = new AnimationObjectOpacity(*this, object, length, direction);
	AddAnimation(animation);
	StartAnimation(animation);
	return animation;
}
