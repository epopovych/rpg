#pragma once

#include <string>

namespace Graphics 
{
	namespace Implementation 
	{
		class IRenderingContext;

		class TextFont
		{
		public:
			TextFont(const std::wstring& fontFamilyName, float fontSize, IRenderingContext& context);

			std::wstring& FontFamilyName();			

			float FontSize() const;
			void SetFontSize(float size);

			DX::DirectWrite::TextFormat& textFormat();

		private:
			void CreateTextFormat(IRenderingContext& context);

			std::wstring m_fontFamilyName;
			float m_fontSize;

			DX::DirectWrite::TextFormat m_format;
		};
	}
}