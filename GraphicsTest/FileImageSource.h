#pragma once

#include <string>

#include "ImageSource.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class FileImageSource : public ImageSource
		{
		public:
			FileImageSource(const std::wstring& filename);

			virtual DX::Direct2D::Bitmap1 GetBitmap(IRenderingContext* context) override;

		private:
			DX::Direct2D::Bitmap1 LoadImageFromFile(IRenderingContext* context, const std::wstring& filename);

			std::wstring m_filename;
			DX::Direct2D::Bitmap1 m_d2dBitmap;
		};
	}
}

