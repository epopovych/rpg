#pragma once

#include <string>

#include "Object.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class TextFont;

		class Text : public Object
		{
		public:
			explicit Text(TextFont& font);

			std::wstring Content() const;
			void SetContent(const std::wstring& content);

			DX::Color ForegroundColor() const;
			void SetForegroundColor(const DX::Color& color);

			float Width() const;
			void SetWidth(float width);

			virtual void CreateDeviceResources(IRenderingContext* context) override;
			virtual void Render(IRenderingContext* context) override;

		private:
			std::wstring m_content;
			TextFont& m_font;
			DX::Direct2D::SolidColorBrush m_foregroundBrush;
			float m_width;
			DX::Color m_foregroundColor;
		};
	}
}