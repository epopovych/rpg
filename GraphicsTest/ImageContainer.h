#pragma once

#include <memory>
#include <vector>

#include "ImageSource.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class ImageContainer : public Object
		{
		public:
			ImageContainer();

			void AddImageSource(std::unique_ptr<ImageSource>& source);

			int GetCount() const;
			
			int GetCurrentIndex() const;
			void SetCurrentIndex(int index);

			virtual void Render(IRenderingContext* context) override;

		private:
			bool UpdateCurrentBitmap(IRenderingContext* context);

			int m_currentIndex;
			DX::Direct2D::Bitmap1 m_d2dCurrentBitmap;
			std::vector<std::unique_ptr<ImageSource>> m_sources;
		};
	}
}