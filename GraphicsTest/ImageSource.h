#pragma once

#include "RenderingContext.h"

namespace Graphics 
{
	namespace Implementation 
	{
		class ImageSource
		{
		public:
			virtual DX::Direct2D::Bitmap1 GetBitmap(IRenderingContext* context) = 0;
		};
	}
}