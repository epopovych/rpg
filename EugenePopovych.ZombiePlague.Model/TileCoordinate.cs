﻿using System;

namespace EugenePopovych.ZombiePlague.Model
{
    public class TileCoordinate
    {
        public TileCoordinate(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public TileCoordinate(TileCoordinate other)
        {
            Row = other.Row;
            Column = other.Column;
        }

        public int Row { get; set; }
        public int Column { get; set; }

        public bool IsAdjacent(TileCoordinate other)
        {
            int rowDistance = Math.Abs(this.Row - other.Row);
            int columnDistance = Math.Abs(this.Column - other.Column);

            bool adjacentVertically = rowDistance == 1;
            bool adjacentHorizontally = columnDistance == 1;

            return adjacentHorizontally ^ adjacentVertically;
        }

        public TileCoordinate GetNextPosition(Direction direction)
        {
            TileCoordinate nextPosition = new TileCoordinate(this);
            switch (direction)
            {
                case Direction.North:
                    nextPosition.Column += 1;
                    break;
                case Direction.East:
                    nextPosition.Row += 1;
                    break;
                case Direction.South:
                    nextPosition.Column -= 1;
                    break;
                case Direction.West:
                    nextPosition.Row -= 1;
                    break;
            }
            return nextPosition;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", Row, Column);
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            if (obj.GetType() != GetType())
                return false;

            var other = (TileCoordinate)obj;
            return this.Row == other.Row && this.Column == other.Column;
        }

        public override int GetHashCode()
        {
            return Row.GetHashCode() ^ Column.GetHashCode();
        }
    }
}
