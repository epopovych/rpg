﻿namespace EugenePopovych.ZombiePlague.Model
{
    public abstract class Action
    {
        protected Game _game;

        protected Action(Game game)
        {
            _game = game;
        }

        public string Name { get; protected set; }

        public abstract int ActionPointsPrice { get; }

        public void Execute()
        {
            ExecuteInternal();
            
            _game.CurrentCharacter.CurrentActionPoints -= ActionPointsPrice;
            if (_game.CurrentCharacter.CurrentActionPoints == 0)
                _game.SetNextCurrentCharacter();
        }

        protected abstract void ExecuteInternal();
    }
}
