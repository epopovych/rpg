﻿using System;
using System.Collections.Generic;

namespace EugenePopovych.ZombiePlague.Model
{
    public class Character
    {
        private TileCoordinate _position = new TileCoordinate(0, 0);

        public Character(string name)
        {
            Name = name;
            DefaultActionPoints = 4;
        }

        public string Name { get; set; }

        public int DefaultActionPoints { get; set; }

        public int CurrentActionPoints { get; set; }

        public TileCoordinate Position { 
            get { return _position; } 
            set { _position = value; } 
        }

        public Direction Direction { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public IEnumerable<Action> GetActions(Game game)
        {
            yield return new MoveForwardAction(game);
        }
    }
}
