﻿using System;
using System.Collections.Generic;

namespace EugenePopovych.ZombiePlague.Model
{
    public class Game
    {
        private readonly List<Character> _characters = new List<Character>();

        private readonly Board _board = new Board(1, 1);

        public Board Board { get { return _board; } }

        public IList<Character> Characters { get { return _characters; } }

        public Character CurrentCharacter { get; private set; }

        public void SetNextCurrentCharacter()
        {
            if (_characters.Count == 0)
                throw new InvalidOperationException("Characters collection is empty.");

            CurrentCharacter = GetNextCharacter();

            CurrentCharacter.CurrentActionPoints = CurrentCharacter.DefaultActionPoints;
        }

        private Character GetNextCharacter()
        {
            int index = _characters.IndexOf(CurrentCharacter);
            index = index < _characters.Count - 1 ? index + 1 : 0;
            return Characters[index];
        }
    }
}
