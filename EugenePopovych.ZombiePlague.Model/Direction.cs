﻿namespace EugenePopovych.ZombiePlague.Model
{
    public enum Direction
    {
        North,
        West, 
        South,
        East
    }
}