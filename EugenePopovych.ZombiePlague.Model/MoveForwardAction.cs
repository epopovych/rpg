﻿using System;
using System.Linq;

namespace EugenePopovych.ZombiePlague.Model
{
    public class MoveForwardAction : Action
    {
        public MoveForwardAction(Game game)
            : base(game)
        {
            Name = "Move Forward";
        }

        public override int ActionPointsPrice
        {
            get { return 1; }
        }

        protected override void ExecuteInternal()
        {
            var board = _game.Board;
            var character = _game.CurrentCharacter;

            TileCoordinate nextPosition = character.Position.GetNextPosition(character.Direction);

            if (CanGoToTile(nextPosition))
            {
                character.Position = nextPosition;
            }
        }

        private bool IsTileFree(TileCoordinate nextPosition)
        {
            return !_game.Characters.Any(_ => _.Position.Equals(nextPosition));
        }

        private bool CanGoToTile(TileCoordinate position)
        {
            var board = _game.Board;
            var character = _game.CurrentCharacter;

            return (!board.IsBorderImpassable(character.Position, position) &&
                    IsTileFree(position) &&
                    !board.IsObstacle(position));

        }
    }
}
