﻿using System;
using System.Collections.Generic;

namespace EugenePopovych.ZombiePlague.Model
{
    public class Board
    {
        public Board(int width, int height)
        {
            Width = width;
            Height = height;
        }

        private int _width;
        public int Width {
            get
            {
                return _width;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("Width cannot be equal or less than 0.");

                _width = value;
            }
        }

        private int _height;
        public int Height
        {
            get
            {
                return _height;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("Width cannot be equal or less than 0.");

                _height = value;
            }
        }

        private readonly HashSet<Tuple<TileCoordinate, TileCoordinate>> _impassableBorders = new HashSet<Tuple<TileCoordinate, TileCoordinate>>();
        private readonly HashSet<TileCoordinate> _obstacles = new HashSet<TileCoordinate>();
        
        public void MarkBorderAsImpassable(TileCoordinate tile1, TileCoordinate tile2)
        {
            if (!tile1.IsAdjacent(tile2))
                throw new ArgumentException("Tiles are not adjacent.");

            _impassableBorders.Add(Tuple.Create(tile1, tile2));
        }

        public bool IsBorderImpassable(TileCoordinate tile1, TileCoordinate tile2)
        {
            return _impassableBorders.Contains(Tuple.Create(tile1, tile2));
        }

        public void MarkTileAsObstacle(TileCoordinate tile)
        {
            _obstacles.Add(tile);
        }

        public bool IsObstacle(TileCoordinate tile)
        {
            return _obstacles.Contains(tile);
        }
    }
}
