#pragma once

#include <UIAnimation.h>

#include <vector>

#include "DirectXBase.h"

ref class BitmapRenderer sealed : public DirectXBase
{
public:
	BitmapRenderer();

	// DirectXBase methods.
	virtual void CreateDeviceIndependentResources() override;
	virtual void CreateDeviceResources() override;
	virtual void CreateWindowSizeDependentResources() override;
	virtual void Render() override;

	void MovePosition(int x, int y);
	
	void StartInfiniteAnimation();

private:	
	void CreateJonesMovesAnimation();
	void CreateTextFormat();

	static const UI_ANIMATION_SECONDS DURATION;
	static const double ACCELERATION_RATIO;
	static const double DECCELERATION_RATIO;
	static const double VELOCITY;
	static const double DELTA;

	Microsoft::WRL::ComPtr<ID2D1Bitmap> m_bitmap;
	int m_x;
	int m_y;
	int m_bitmapIndex;

	std::vector<Microsoft::WRL::ComPtr<ID2D1Bitmap>> m_arrayBitmaps;

	Microsoft::WRL::ComPtr<IUIAnimationManager> m_animationManager;
	Microsoft::WRL::ComPtr<IUIAnimationTimer> m_animationTimer;
	Microsoft::WRL::ComPtr<IUIAnimationTransitionLibrary> m_animationTransitionLibrary;
	Microsoft::WRL::ComPtr<IUIAnimationVariable> m_animationVarX;
	Microsoft::WRL::ComPtr<IUIAnimationVariable> m_animationVarY;
	Microsoft::WRL::ComPtr<IUIAnimationVariable> m_animationVarIndex;

	Microsoft::WRL::ComPtr<IDWriteTextFormat> m_textFormat;
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> m_textForegroundBrush;
};