#include "pch.h"
#include "BitmapRenderer.h"
#include "AnimationManagerEventHandler.h"

#include <iostream>
#include <algorithm>
#include <sstream>

const UI_ANIMATION_SECONDS BitmapRenderer::DURATION = 1.0;
const double BitmapRenderer::ACCELERATION_RATIO = 0.4;
const double BitmapRenderer::DECCELERATION_RATIO = 0.4;
const double BitmapRenderer::VELOCITY = 11.0;
const double BitmapRenderer::DELTA = 500.0;

BitmapRenderer::BitmapRenderer(void)
{
	m_x = m_y = 10;
	m_bitmapIndex = 0;
}

void BitmapRenderer::CreateDeviceIndependentResources()
{
	__super::CreateDeviceIndependentResources();

	// animation initialization
	
	DX::ThrowIfFailed(
		CoCreateInstance(
			CLSID_UIAnimationManager,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(m_animationManager.GetAddressOf())
			)
		);

	DX::ThrowIfFailed(
		CoCreateInstance(
			CLSID_UIAnimationTimer,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(m_animationTimer.GetAddressOf())
			)
		);

	DX::ThrowIfFailed(
		CoCreateInstance(
			CLSID_UIAnimationTransitionLibrary,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(m_animationTransitionLibrary.GetAddressOf())
			)
		);

	Microsoft::WRL::ComPtr<IUIAnimationManagerEventHandler> eventHandler = 
		Microsoft::WRL::Make<AnimationManagerEventHandler>(this);

	m_animationManager->SetManagerEventHandler(eventHandler.Get());
	//m_animationManager->CreateAnimationVariable(m_x, m_animationVarX.GetAddressOf());
	//m_animationManager->CreateAnimationVariable(m_y, m_animationVarY.GetAddressOf());
	//
	
	CreateTextFormat();
}

void BitmapRenderer::CreateDeviceResources()
{
	__super::CreateDeviceResources();

	//DX::ThrowIfFailed(
	//	LoadBitmapFromFile(".\\Assets\\Character\\jonesA.png", 0, 0, m_bitmap)
	//	);

	CreateJonesMovesAnimation();	

	DX::ThrowIfFailed(
		m_d2dContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), 
											m_textForegroundBrush.GetAddressOf())
		);
}

void BitmapRenderer::CreateWindowSizeDependentResources()
{
	__super::CreateWindowSizeDependentResources();
}

void BitmapRenderer::Render()
{
	m_d2dContext->BeginDraw();

	m_d2dContext->Clear(D2D1::ColorF(D2D1::ColorF::DodgerBlue));

	if (m_animationVarIndex.Get() != nullptr) {	
		UI_ANIMATION_SECONDS now;
		DX::ThrowIfFailed(
			m_animationTimer->GetTime(&now)
			);
	
		DX::ThrowIfFailed(
			m_animationManager->Update(now)
			);
	
		DX::ThrowIfFailed(
			m_animationVarX->GetIntegerValue(&m_x)
			);
		//DX::ThrowIfFailed(
		//	m_animationVarY->GetIntegerValue(&m_y)
		//	);
		DX::ThrowIfFailed(
			m_animationVarIndex->GetIntegerValue(&m_bitmapIndex)
			);
	
		m_bitmapIndex = m_bitmapIndex % m_arrayBitmaps.size();
		m_bitmap = m_arrayBitmaps[m_bitmapIndex];
	
		auto bitmapSize = m_bitmap->GetSize();	
		D2D1_RECT_F destRect;
		destRect.left = static_cast<float>(m_x);
		destRect.top = static_cast<float>(m_y);
		destRect.right = destRect.left + bitmapSize.width;
		destRect.bottom = destRect.top + bitmapSize.height;
		m_d2dContext->DrawBitmap(m_bitmap.Get(), destRect);
	
		double varIndexValue;
		m_animationVarIndex->GetValue(&varIndexValue);
		std::wstringstream s;
		s << L"m_animationVarIndex = " << varIndexValue << L"; m_bitmapIndex = " << m_bitmapIndex << L"; now = " << now;
		auto formattedString = s.str();
		m_d2dContext->DrawText(formattedString.c_str(), formattedString.size(), m_textFormat.Get(),
			D2D1::RectF(800.0, 10.0, 1600.0, 250.0), m_textForegroundBrush.Get());
	}

	HRESULT hr = m_d2dContext->EndDraw();
	if (hr != D2DERR_RECREATE_TARGET)
	{
		DX::ThrowIfFailed(hr);
	}

	m_renderNeeded = false;

	UI_ANIMATION_MANAGER_STATUS status;
	DX::ThrowIfFailed(
		m_animationManager->GetStatus(&status)
		);
	if (status == UI_ANIMATION_MANAGER_BUSY)
	{
		m_renderNeeded = true;
	}
}

void BitmapRenderer::MovePosition(int x, int y)
{
	// start animation
	//m_position.x += x;
	//m_position.y += y;

	Microsoft::WRL::ComPtr<IUIAnimationStoryboard> storyboard;
	DX::ThrowIfFailed(
		m_animationManager->CreateStoryboard(storyboard.GetAddressOf())
		);

	Microsoft::WRL::ComPtr<IUIAnimationTransition> transitionX;
	DX::ThrowIfFailed(
		m_animationTransitionLibrary->CreateAccelerateDecelerateTransition(
			DURATION,
			m_x + x,
			ACCELERATION_RATIO,
			DECCELERATION_RATIO,
			transitionX.GetAddressOf())
		);

	Microsoft::WRL::ComPtr<IUIAnimationTransition> transitionY;
	DX::ThrowIfFailed(
		m_animationTransitionLibrary->CreateAccelerateDecelerateTransition(
			DURATION,
			m_y + y,
			ACCELERATION_RATIO,
			DECCELERATION_RATIO,
			transitionY.GetAddressOf())
		);

	DX::ThrowIfFailed(
		storyboard->AddTransition(m_animationVarX.Get(), transitionX.Get())
		);
	DX::ThrowIfFailed(
		storyboard->AddTransition(m_animationVarY.Get(), transitionY.Get())
		);

	UI_ANIMATION_SECONDS now;
	DX::ThrowIfFailed(
		m_animationTimer->GetTime(&now)
		);

	DX::ThrowIfFailed(
		storyboard->Schedule(now)
		);
}

void BitmapRenderer::StartInfiniteAnimation()
{
	Microsoft::WRL::ComPtr<IUIAnimationStoryboard> storyboard;
	DX::ThrowIfFailed(
		m_animationManager->CreateStoryboard(storyboard.GetAddressOf())
		);

	DX::ThrowIfFailed(
		m_animationManager->CreateAnimationVariable(m_x, m_animationVarX.GetAddressOf())
		);

	DX::ThrowIfFailed(
		m_animationManager->CreateAnimationVariable(0.0f, m_animationVarIndex.GetAddressOf())
		);
	m_animationVarIndex->SetRoundingMode(UI_ANIMATION_ROUNDING_CEILING);

	Microsoft::WRL::ComPtr<IUIAnimationTransition> transitionCoord;
	DX::ThrowIfFailed(
		m_animationTransitionLibrary->CreateLinearTransitionFromSpeed(
			VELOCITY, 
			m_x + DELTA, 
			transitionCoord.GetAddressOf())
		);

	Microsoft::WRL::ComPtr<IUIAnimationTransition> transitionIndex;
	DX::ThrowIfFailed(
		m_animationTransitionLibrary->CreateLinearTransitionFromSpeed(
			VELOCITY, 500.0, // some big value. it's expected to be cut by keyframes with velocity unchanged.
			transitionIndex.GetAddressOf())
		);

	DX::ThrowIfFailed(
		storyboard->AddTransition(m_animationVarX.Get(), transitionCoord.Get())
		);

	UI_ANIMATION_KEYFRAME finishKeyframe;
	DX::ThrowIfFailed(
		storyboard->AddKeyframeAfterTransition(transitionCoord.Get(), &finishKeyframe)
		);

	DX::ThrowIfFailed(
		storyboard->AddTransitionBetweenKeyframes(
			m_animationVarIndex.Get(), 
			transitionIndex.Get(), 
			UI_ANIMATION_KEYFRAME_STORYBOARD_START,
			finishKeyframe)
		);

	//DX::ThrowIfFailed(
	//	storyboard->RepeatBetweenKeyframes(UI_ANIMATION_KEYFRAME_STORYBOARD_START, finishKeyframe, UI_ANIMATION_REPEAT_INDEFINITELY)
	//	);

	UI_ANIMATION_SECONDS now;
	DX::ThrowIfFailed(
		m_animationTimer->GetTime(&now)
		);

	DX::ThrowIfFailed(
		storyboard->Schedule(now)
		);
}

void BitmapRenderer::CreateJonesMovesAnimation()
{
	m_arrayBitmaps.clear();	

	std::vector<Platform::String^> fileNames;
	fileNames.push_back(".\\Assets\\Character\\jm0.png");
	fileNames.push_back(".\\Assets\\Character\\jm01.png");
	fileNames.push_back(".\\Assets\\Character\\jm02.png");
	fileNames.push_back(".\\Assets\\Character\\jm03.png");
	fileNames.push_back(".\\Assets\\Character\\jm04.png");
	fileNames.push_back(".\\Assets\\Character\\jm05.png");
	fileNames.push_back(".\\Assets\\Character\\jm06.png");
	fileNames.push_back(".\\Assets\\Character\\jm07.png");
	fileNames.push_back(".\\Assets\\Character\\jm08.png");
	fileNames.push_back(".\\Assets\\Character\\jm09.png");
	fileNames.push_back(".\\Assets\\Character\\jm10.png");
	fileNames.push_back(".\\Assets\\Character\\jm11.png");

	m_arrayBitmaps.resize(fileNames.size());

	std::transform(std::begin(fileNames), std::end(fileNames), std::begin(m_arrayBitmaps),
			[&](Platform::String^ fileName)
		{
			Microsoft::WRL::ComPtr<ID2D1Bitmap> bitmap;
			LoadBitmapFromFile(fileName, 0, 0, bitmap);
			return bitmap;
		});
}

void BitmapRenderer::CreateTextFormat()
{
	const WCHAR* fontFamilyName = L"Verdana";
	const FLOAT fontSize = 18.0;

	DX::ThrowIfFailed(
		m_dwriteFactory->CreateTextFormat(
			fontFamilyName, 
			nullptr, 
			DWRITE_FONT_WEIGHT_NORMAL,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			fontSize,
			L"",
			m_textFormat.GetAddressOf())
		);
}
