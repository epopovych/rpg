#pragma once

#include <wrl.h>
#include <UIAnimation.h>

#include "DirectXPage.xaml.h"

using Microsoft::WRL::RuntimeClass;
using Microsoft::WRL::RuntimeClassFlags;

class AnimationManagerEventHandler : public 
	RuntimeClass<RuntimeClassFlags<Microsoft::WRL::ClassicCom>, 
				 IUIAnimationManagerEventHandler>
{
public:
	AnimationManagerEventHandler(DirectXBase^ renderer)
	{
		m_renderer = renderer;
	}

	STDMETHODIMP OnManagerStatusChanged( 
		/* [annotation][in] */ 
		_In_  UI_ANIMATION_MANAGER_STATUS newStatus,
		/* [annotation][in] */ 
		_In_  UI_ANIMATION_MANAGER_STATUS previousStatus)
	{
		HRESULT hr = S_OK;
		if (newStatus == UI_ANIMATION_MANAGER_BUSY)
		{
			m_renderer->Invalidate();
		}
		return hr;
	}

private:
	DirectXBase^ m_renderer;
};
