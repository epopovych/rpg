#include <wrl.h>
#include <atlbase.h>
#include <atlwin.h>

#include <d2d1_1.h>
#include <d2d1helper.h>
#include <d3d11_1.h>

using namespace Microsoft::WRL;
using namespace D2D1;

#pragma comment(lib, "d2d1")
#pragma comment(lib, "d3d11")

#include "Hr.h"