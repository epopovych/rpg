#pragma once

#include "Pch.h"

template <typename T>
struct DesktopWindow :
	CWindowImpl<DesktopWindow<T>,
	CWindow,
	CWinTraits<WS_OVERLAPPEDWINDOW | WS_VISIBLE>>
{
	ComPtr<ID2D1DeviceContext> m_target;
	ComPtr<IDXGISwapChain1> m_swapChain;
	ComPtr<ID2D1Factory1> m_factory;

	DECLARE_WND_CLASS_EX(nullptr, 0, -1);

	BEGIN_MSG_MAP(c)
		MESSAGE_HANDLER(WM_PAINT, PaintHandler)
		MESSAGE_HANDLER(WM_SIZE, SizeHandler)
		MESSAGE_HANDLER(WM_DISPLAYCHANGE, DisplayChangeHandler)
		MESSAGE_HANDLER(WM_DESTROY, DestroyHandler)
	END_MSG_MAP()

	LRESULT PaintHandler(UINT, WPARAM, LPARAM, BOOL &)
	{
		PAINTSTRUCT ps;
		VERIFY(BeginPaint(&ps));
		//Render();
		EndPaint(&ps);
		return 0;
	}

	LRESULT DisplayChangeHandler(UINT, WPARAM, LPARAM, BOOL &)
	{
		//Render();
		return 0;
	}

	LRESULT SizeHandler(UINT, WPARAM wparam, LPARAM, BOOL &)
	{
		if (m_target && SIZE_MINIMIZED != wparam)
		{
			ResizeSwapChainBitmap();
			Render();
		}
		return 0;
	}

	LRESULT DestroyHandler(UINT, WPARAM, LPARAM, BOOL &)
	{
		PostQuitMessage(0);
		return 0;
	}

	void Run()
	{
		D2D1_FACTORY_OPTIONS fo = {};
#ifdef _DEBUG
		fo.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif
		HR(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,
			fo,
			m_factory.GetAddressOf()));
		static_cast<T *>(this)->CreateDeviceIndependentResources();
		VERIFY(__super::Create(nullptr, nullptr, "Introducing Direct2D 1.1"));
		MSG message;
		BOOL result;
		while (result = GetMessage(&message, 0, 0, 0))
		{
			if (-1 != result)
			{
				DispatchMessage(&message);
			}
		}
	}
};