#pragma once

#define ASSERT ATLASSERT
#define VERIFY ATLVERIFY
#ifdef _DEBUG
#define HR(expression) ASSERT(S_OK == (expression))
#else
struct ComException
{
	HRESULT const hr;
	ComException(HRESULT const value) : hr(value) {}
};
inline void HR(HRESULT const hr)
{
	if (S_OK != hr) throw ComException(hr);
}
#endif