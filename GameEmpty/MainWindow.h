#pragma once

#include "DesktopWindow.h"

class MainWindow : public DesktopWindow<MainWindow>
{
public:
	MainWindow(void);
	~MainWindow(void);

	HRESULT CreateDeviceIndependentResources();
	HRESULT CreateDeviceResources();
	void ReleaseDevice();
	void ResizeSwapChainBitmap();
	void Render();
};

