﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class ActionTest
    {
        private class ActionStub : Action
        {
            public ActionStub(Game game)
                : base(game)
            {
            }

            public override int ActionPointsPrice
            {
                get { return 1; }
            }

            protected override void ExecuteInternal()
            {                
            }
        }

        [TestMethod]
        public void Execute_AfterExecuted_CurrentCharacterSpendActionPointsPrice()
        {
            var game = new Game();
            var character = new Character("character");
            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            int oldAP = character.CurrentActionPoints;

            var action = new ActionStub(game);
            action.Execute();

            Assert.AreEqual(oldAP - action.ActionPointsPrice, character.CurrentActionPoints);
        }

        [TestMethod]
        public void Execute_WhenNoActionPointsAreLeft_NextCharacterBecomesCurrent()
        {
            var game = new Game();
            var character1 = new Character("character1");
            var character2 = new Character("character2");
            game.Characters.Add(character1);
            game.Characters.Add(character2);
            game.SetNextCurrentCharacter();

            var action = new ActionStub(game);
            action.Execute();
            action.Execute();
            action.Execute();
            action.Execute();

            Assert.AreEqual(character2, game.CurrentCharacter);
        }
    }
}
