﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class CharacterTest
    {
        [TestMethod]
        public void Ctor_NameIsSetProperly()
        {
            string expectedName = "Character";
        	var char1 = new Character(expectedName);

            Assert.AreEqual(expectedName, char1.Name);
        }

        [TestMethod]
        public void GetActions_Always_ContainsMoveForward()
        {
            Character character = new Character("character 1");
            Game game = new Game();
            var actions = character.GetActions(game);

            Assert.IsTrue(actions.Any(_ => _.GetType().Equals(typeof(MoveForwardAction))));
        }
    }
}
