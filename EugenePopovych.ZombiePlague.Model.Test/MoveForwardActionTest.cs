﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class MoveForwardActionTest
    {
        [TestMethod]
        public void Execute_CharacterDirectionIsNorth_MovesCharacterForOneTileNorth()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;
            
            Character character = new Character("character 1");
            character.Position.Row = 2;
            character.Position.Column = 2;
            character.Direction = Direction.North;

            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(2, 3);
            Assert.AreEqual(expected, character.Position);
        }

        [TestMethod]
        public void Execute_CharacterDirectionIsWest_MovesCharacterForOneTileWest()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;
            
            Character character = new Character("character 1");
            character.Position.Row = 2;
            character.Position.Column = 2;
            character.Direction = Direction.West;

            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(1, 2);
            Assert.AreEqual(expected, character.Position);
        }

        [TestMethod]
        public void Execute_CharacterDirectionIsSouth_MovesCharacterForOneTileSouth()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;

            Character character = new Character("character 1");
            character.Position.Row = 2;
            character.Position.Column = 2;
            character.Direction = Direction.South;

            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(2, 1);
            Assert.AreEqual(expected, character.Position);
        }

        [TestMethod]
        public void Execute_CharacterDirectionIsEast_MovesCharacterForOneTileEast()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;

            Character character = new Character("character 1");
            character.Position.Row = 2;
            character.Position.Column = 2;
            character.Direction = Direction.East;
            
            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(3, 2);
            Assert.AreEqual(expected, character.Position);
        }

        [TestMethod]
        public void Execute_WhenBorderIsImpassable_NothingHappens()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;
            game.Board.MarkBorderAsImpassable(new TileCoordinate(2, 2), new TileCoordinate(3, 2));

            Character character = new Character("character 1");
            character.Position.Row = 2;
            character.Position.Column = 2;
            character.Direction = Direction.East;

            game.Characters.Add(character);
            game.SetNextCurrentCharacter();

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(2, 2);
            Assert.AreEqual(expected, character.Position);
        }

        [TestMethod]
        public void Execute_WhenNextTileContainsCharacter_NothingHappens()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;

            Character character1 = new Character("character 1");
            Character character2 = new Character("character 2");
            game.Characters.Add(character1);
            game.Characters.Add(character2);
            game.SetNextCurrentCharacter();

            character1.Position.Row = 2;
            character1.Position.Column = 2;
            character1.Direction = Direction.North;

            character2.Position.Row = 2;
            character2.Position.Column = 3;

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(2, 2);
            Assert.AreEqual(expected, character1.Position);
        }

        [TestMethod]
        public void Execute_WhenNextTileMarkedAsObstacle_NothingHappens()
        {
            Game game = new Game();
            game.Board.Width = game.Board.Height = 10;

            Character character1 = new Character("character 1");
            game.Characters.Add(character1);
            game.SetNextCurrentCharacter();

            character1.Position.Row = 2;
            character1.Position.Column = 2;
            character1.Direction = Direction.North;

            game.Board.MarkTileAsObstacle(new TileCoordinate(2, 3));

            var action = new MoveForwardAction(game);
            action.Execute();

            var expected = new TileCoordinate(2, 2);
            Assert.AreEqual(expected, character1.Position);
        }
    }
}
