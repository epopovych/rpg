﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class GameTest
    {
        // initialize (add characters, add items, add weapons, create border)

        #region set current character

        [TestMethod]
        public void SetNextCurrentCharacter_WhenNothingIsSelected_FirstIsSelected()
        {
            Game game = new Game();
            Character char1 = new Character("character 1");
            Character char2 = new Character("character 2");
            game.Characters.Add(char1);
            game.Characters.Add(char2);

            game.SetNextCurrentCharacter();

            Assert.AreEqual(char1, game.CurrentCharacter);
        }

        [TestMethod]
        public void SetNextCurrentCharacter_WhenFirstIsSelected_SecondIsSelected()
        {
            Game game = new Game();
            Character char1 = new Character("character 1");
            Character char2 = new Character("character 2");
            game.Characters.Add(char1);
            game.Characters.Add(char2);         

            game.SetNextCurrentCharacter();
            game.SetNextCurrentCharacter();

            Assert.AreEqual(char2, game.CurrentCharacter);
        }

        [TestMethod]
        public void SetNextCurrentCharacter_WhenLastIsSelected_FirstIsSelected()
        {
            Game game = new Game();
            Character char1 = new Character("character 1");
            Character char2 = new Character("character 2");
            game.Characters.Add(char1);
            game.Characters.Add(char2);

            game.SetNextCurrentCharacter();
            game.SetNextCurrentCharacter();
            game.SetNextCurrentCharacter();

            Assert.AreEqual(char1, game.CurrentCharacter);
        }

        [TestMethod]
        public void SetNextCurrentCharacter_WhenNoCharacters_InvalidOperationIsThrown()
        {
            Game game = new Game();

            TestHelper.AssertExceptionExpected<InvalidOperationException>(() =>
                {
                    game.SetNextCurrentCharacter();
                });
        }

        #endregion // set current character

        #region set action points for current player 

        [TestMethod]
        public void SetNextCurrentCharacter_AfterExecuted_CharacterCurrentActionPointsIsSetToDefault()
        {
            var game = new Game();
            var character = new Character("character");
            game.Characters.Add(character);

            game.SetNextCurrentCharacter();

            Assert.AreEqual(character.DefaultActionPoints, character.CurrentActionPoints);
        }

        #endregion

        // execute actions
        // check victory conditions
        // finish turn and set next current player
    }
}
