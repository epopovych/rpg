﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class TileCoordinateTest
    {
        [TestMethod]
        public void IsAdjacent_ForAdjacentTile_ReturnsTrue()
        {
            var tile1 = new TileCoordinate(0, 0);
            var tile2 = new TileCoordinate(0, 1);

            Assert.IsTrue(tile1.IsAdjacent(tile2));
        }

        [TestMethod]
        public void IsAdjacent_ForNotAdjacentTile_ReturnsFalse()
        {
            var tile1 = new TileCoordinate(0, 0);
            var tile2 = new TileCoordinate(0, 2);

            Assert.IsFalse(tile1.IsAdjacent(tile2));
        }

        [TestMethod]
        public void IsAdjacent_ForAdjacentDiagonally_ReturnsFalse()
        {
            var tile1 = new TileCoordinate(0, 0);
            var tile2 = new TileCoordinate(1, 1);

            Assert.IsFalse(tile1.IsAdjacent(tile2));
        }

        [TestMethod]
        public void IsAdjacent_ForSameTile_ReturnsFalse()
        {
            var tile1 = new TileCoordinate(1, 1);
            var tile2 = new TileCoordinate(1, 1);

            Assert.IsFalse(tile1.IsAdjacent(tile2));
        }

        [TestMethod]
        public void IsAdjacent_WhenExchangeOperands_ReturnsTheSame()
        {
            var tile1 = new TileCoordinate(1, 1);
            var tile2 = new TileCoordinate(1, 2);

            var result1 = tile1.IsAdjacent(tile2);
            var result2 = tile2.IsAdjacent(tile1);

            Assert.AreEqual(result1, result2);
        }
    }
}
