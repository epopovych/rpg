﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    internal class TestHelper
    {
        internal static void AssertExceptionExpected<T>(System.Action test) where T : Exception
        {
            try
            {
                test();
            }
            catch (T)
            {
                return;
            }
            catch (Exception e)
            {
                Assert.Fail("{0} has been thrown while {1} was expected", e.GetType(), typeof(T));
            }
            Assert.Fail("No exception has been thrown while {0} was expected", typeof(T));
        }
    }
}
