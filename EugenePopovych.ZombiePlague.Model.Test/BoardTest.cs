﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace EugenePopovych.ZombiePlague.Model.Test
{
    [TestClass]
    public class BoardTest
    {
        #region Width and height tests

        [TestMethod]
        public void ConstructorWidthCannotBeNegative()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(-100, 100);
            });
        }

        [TestMethod]
        public void ConstructorWidthCannotBeZero()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(0, 100);
            });
        }

        [TestMethod]
        public void ConstructorHeightCannotBeNegative()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, -100);
            });
        }

        [TestMethod]
        public void ConstructorHeightCannotBeZero()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, 0);
            });
        }

        [TestMethod]
        public void WidthCannotBeNegative()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, 100);
                board.Width = -100;
            });
        }

        [TestMethod]
        public void WidthCannotBeZero()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, 100);
                board.Width = 0;
            });
        }

        [TestMethod]
        public void HeightCannotBeNegative()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, 100);
                board.Height = -100;
            });
        }

        [TestMethod]
        public void HeightCannotBeZero()
        {
            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
            {
                Board board = new Board(100, 100);
                board.Height = 0;
            });
        }

        #endregion //Width and height tests

        #region Mark border of two tiles as impassable

        [TestMethod]
        public void MarkBorderAsImpassable_WhenBorderMarked_IsBorderMarkedImpassableReturnsTrue()
        {
            Board board = new Board(10, 10);
            var tile1 = new TileCoordinate(2, 2);
            var tile2 = new TileCoordinate(2, 3);
            
            board.MarkBorderAsImpassable(tile1, tile2);

            Assert.IsTrue(board.IsBorderImpassable(tile1, tile2));
        }

        [TestMethod]
        public void MarkBorderAsImpassable_WhenTilesAreNotAdjacent_ThrowsException()
        {
            Board board = new Board(10, 10);

            TestHelper.AssertExceptionExpected<ArgumentException>(() =>
                {
                    board.MarkBorderAsImpassable(new TileCoordinate(0, 0), new TileCoordinate(2, 2));
                });
        }

        #endregion

        #region Test border for impassability

        [TestMethod]
        public void IsBorderImpassable_ForAdjacentTilesNotMarkedImpassable_ReturnsFalse()
        {
            Board board = new Board(5, 5);
            TileCoordinate tile1 = new TileCoordinate(2, 2);
            TileCoordinate tile2 = new TileCoordinate(2, 3);
            TileCoordinate tile3 = new TileCoordinate(3, 2);
            board.MarkBorderAsImpassable(tile1, tile2);

            Assert.IsFalse(board.IsBorderImpassable(tile1, tile3));
        }

        [TestMethod]
        public void IsBorderImpassable_WhenNoTilesMarkedImpassable_ReturnsFalse()
        {
            Board board = new Board(5, 5);
            TileCoordinate tile1 = new TileCoordinate(2, 2);
            TileCoordinate tile2 = new TileCoordinate(2, 3);

            Assert.IsFalse(board.IsBorderImpassable(tile1, tile2));
        }

        #endregion

        #region Mark tile as obstacle

        [TestMethod]
        public void IsObstacle_WhenTileMarkedAsObstacle_ReturnsTrue()
        {
            Board board = new Board(5, 5);
            TileCoordinate tile = new TileCoordinate(2, 2);

            board.MarkTileAsObstacle(tile);

            Assert.IsTrue(board.IsObstacle(tile));
        }

        [TestMethod]
        public void IsObstacle_WhenOtherTileMarkedAsObstacle_ReturnsFalse()
        {
            Board board = new Board(5, 5);
            TileCoordinate tile = new TileCoordinate(2, 2);
            TileCoordinate otherTile = new TileCoordinate(3, 3);

            board.MarkTileAsObstacle(otherTile);

            Assert.IsFalse(board.IsObstacle(tile));
        }

        [TestMethod]
        public void IsObstacle_WhenNoTilesMarkedAsObstacle_ReturnsFalse()
        {
            Board board = new Board(5, 5);
            TileCoordinate tile = new TileCoordinate(2, 2);

            Assert.IsFalse(board.IsObstacle(tile));        	
        }

        #endregion
    }
}
