#pragma once

namespace Graphics
{
	// wrapper for DirectX Point2F type to use it in ABI. can we avoid that? are there any standard types?
	public value struct Point2d : Platform::ValueType
	{	
		double x;
		double y;
	};
}