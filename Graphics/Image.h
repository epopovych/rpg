#pragma once

#include "Point2d.h"
#include "GraphicObject.h"

namespace Graphics
{	
	public ref class Image sealed
	{
	public:
		Image(void); 
		// TODO: add ctor with image sources as parameters
		virtual ~Image(void);

		property Platform::String^ Filename; // full filename if we want to load image from file
		// TODO: add a source sprite sheet and indices for the image in it

		property Point2d Position;
		//property Size2d Size;

		// TODO: add stretch mode and some filters later if necessary

		//void Render(); // TODO: pass RenderingContext instance as a parameter 		
	};	
}
