#pragma once

#include <collection.h>

namespace Graphics 
{
	ref class RenderingContext;
	ref class GraphicObject;

	public ref class Scene sealed
	{
	public:
		typedef Windows::Foundation::Collections::IVector<Graphics::GraphicObject^>^ GraphicVector;
	public:
		Scene(void);

		property Graphics::RenderingContext^ RenderingContext;

		//property GraphicVector Objects
		//{
		//	GraphicVector get() { return static_cast<GraphicVector>(m_objects); }
		//}

		void CreateDeviceIndependentResources();
		void CreateDeviceResoures();

		void Render();

	private:
		//Platform::Collections::Vector<Graphics::GraphicObject^>^ m_objects;
	};

}