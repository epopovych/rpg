﻿#pragma once

namespace Graphics
{
	ref class RenderingContext;

	public ref class GraphicObject sealed
    {
	internal:
		virtual void Render(RenderingContext^ context);

    private:
        GraphicObject();
    };
}