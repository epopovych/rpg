#pragma once

#include "dx/dx.h"

namespace Graphics
{
	public ref class RenderingContext sealed
	{
	public:
		// TODO: add here initialization interface
	internal:
		// TODO: add here access methods (or fields, or properties?) to Direct2D interfaces
	private:
		// TODO: declare Direct2D interfaces here
		DX::Direct2D::DeviceContext m_deviceContext;
	};
}